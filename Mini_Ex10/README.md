## My individual flowchart

![image](Mini_ex10.png)

I chose to make a flowchart on the latest [mini ex](https://gitlab.com/MattiasJohansen/ap2019/tree/master/Mini_Ex09/p5) about APIs. 
Since I thought it was the most technically advanced while also relevant to this task.

I think that this flowchart shows what happens in the code quite well, since that was my way of going by doing this.
It was a bit difficult to do the flowchart after we had made the program; I thought on how much detail about what is happening in the
code I should put in. If we hade made the flowchart before the program was developed, when we just got the idea, it 
would have been more simplistic. Doing the flowchart this way, made me examine the code more thoroughly, so I could understand and
describe what actually was happening. The wording and the layout is probably what I used the most time on.
Since a flowchart has to be easily understood, it can be tricky to find the best way of guiding the reader through and
communicating the process in a technical, yet understandable manner. How much information that should be put into 
each step/rectangle of a process and how many there should be is something that I changed several times.

## The group ideas
![image](miniex10group.png)

For this week we were assigned to develop two flowcharts in the group. In this phase the point was not to come up with super realistic ideas or super technical ideas either. The point was to try to create flowcharts without having written the code yet. Thus it should be more simple for us when we start coding, since the program is already sketched and divided into steps. 

A challenge will be for us to carry out our ambitions for these flowcharts. Especially the flowchart of the game will have several obstacles along the way. First of all we will probably face difficulties when creating our avatar, as we wish to capture a picture of the user so the user becomes the avatar in the game. To make this happen we must make use of our knowledge from the theme of data capturing using the p5.dom library. We must use a face tracker and identify which parts of the face we want to incorporate in the avatar. It is important that the face moves along with the avatar throughout the whole game.
Another challenge will be to make a magnetic space between the “star”, which symbolises the expectations we meet during our youth, and the avatar, pulling the avatar towards the star. We will try to solve this by making a transparent square around the star and if the avatar is inside this square it will hit the expectations and you will lose happiness in the “happy-meter”. There is some sort of attractive force. This force will make it more difficult for you to reach your life-goals and gain happiness, since expectations will pull you over wherever you go in the arena and lower your happiness if you hit it.
It might also be quite a challenge for us to create a “happy-meter” which keeps lowering when you don’t reach life-goals. We want to lower it by approximately 5% for every five seconds you don’t collect a life-goal to make it more realistic and similar to real life. 

![image](miniex10group02.png)

Our second flowchart shows a more simple  program containing parts we have worked with before. The main challenge will be to continuously query a new word without having to write them over and over again. Also this word has to match with a gif from the API which in this case is GIPHY. Another challenge is how the ball should move around and we will try solving this by looking at one of our colleagues’ codes, and hopefully we can use the same method.

**How is these flowcharts different from the first one, in terms of the role of the flowchart**

These flowcharts both present a way of displaying interaction by pressing buttons; basically
boolean conditional statements via the diamonds shapes. This serves to create the instructions to
a looping program that has many possibilities of reaching an end - or even not never really reaching an
end as in the second group-flow chart.

**If you have to bring the concept of algorithms from flow charts to a wider cultural context, how would you reflect upon the notion of algorithms?**

The flow chart is a way of creating the logic of the algorithm in a way that can be easily understood -
even by people who are not programmers. Thus, flow charts can serve as a communication tool between designers and
programmers. This might be a reason why programmers don't like to make them. The flowchart might give a more holistic
view of the algorithm it creates; it may show the interplay between the decisions that algorithms make: *prioritization, classification, association and filtering.*
The fact that the flow chart is written in a more natural language than the code itself, might show how
the algorithm exert power by deciding what is important and how it is organized and if something is in- or excluded,
rather than just being numbers and syntax that has to be understood on a deeper level.
