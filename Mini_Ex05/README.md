## The Throbber (revisited)
[The animation](https://gl.githack.com/MattiasJohansen/ap2019/raw/master/Mini_Ex5/empty-example/index.html)

![Still image](image.png)



The concept is still a throbber, but I have chosen to merge the weird, unpredictable motions with the rotating motion of more
commonly known trobbers. I wanted to use the for-loop and took some inspiration from other throbbers into my original design,
that was mainly throbbing beacuse of the perlin noise function. I have made it more simple, but also played abit more with the
colors. I made the changes because the "star" formed by the triangles was one of my original ideas when I made the throbber the
first time, inspired by the video game *Portal*, wherein the [Aperture Science logo](https://i1.theportalwiki.net/img/4/43/Logo_aperture.png)
exist. I want to express the opening of a door/portal to the next thing that is loading, while showing the uncomprehensible inner
workings of the machine, by combining the unprecitable noise-motion (and color change) with the predictable rotating motion.

I like to think of my way of coding this piece in the terms of *tinkering* with the code (Bergstrom, I. and Blackwell, A.F. (2016)), 
since I patched some of my previous, code with other functions I learned about and copied from other sources, into my code. By
doing this; changing the parameters and seeing what it does to the piece, I learned how they work, better than when watching 
instructional videos, since I did it myself. I like this approach, and I like that I am aware that it is a "legitimate" approach
to learning. I feel like the digital culture, especially that of the open source community (Processing etc.), is important for people's
ability to learn how to code, since the sharing of code and knowledge makes the skill more accessible. Rather than starting from
scratch, not knowing where to go, being able to just copy random code, that has some guarantee to work, is somehow more rewarding 
in the learner's experience, since you get to play with and expand on something that was created, somewhat skillfully by a 
fellow programmer/stranger from the Internet.