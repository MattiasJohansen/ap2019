let xoff =0.00


function setup() {
  createCanvas(700, 700);
  frameRate(40) //speed of animantion
  strokeJoin(ROUND);//round edges on triagles


}

function draw(){
  background(0) //black background

  xoff = xoff + 0.02; //"noicyness"
    let n = noise(xoff) * width; //n is the chaingen value, that is within the range of the width

  translate(width/2, height/2);
  stroke(0, 450-n*0.8, n) //changing color on strokes
  strokeWeight(8) // strokeWeight will differ in the n-pattern
  noFill()
  ellipse(0,0,540)
  fill(0, n*0.8-150, n) //changing grayscale fill


  rotate(PI / 3.0 + frameCount/60);//speed of rotation
	for (var i = 0; i < 7; i++) { //makes a loop of 7 triagles in the shape of a sphere
		push();
		rotate(TWO_PI * i / 7); //controls the shape of the loop
		triangle(n*0.2-20, n*0.3, 250, 90, 90, 250); //the size and properties of the triangle
		pop();


  }

}
