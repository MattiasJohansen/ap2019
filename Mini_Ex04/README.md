https://gl.githack.com/MattiasJohansen/ap2019/raw/master/Mini_Ex4/index.html

NB:
1. It takes a long time to load in the link, actually just download the files and display in the live-server, it'll be faster.
2. Use Mozilla Firefox, allow webcam.
3. Don't use glasses, dont move around to much.

![image](image.png)

### Description

My sketch is a blank background with curves resembling a few facial characteristics of the person captured by the
webcam. As the person moves, the displayed face moves around, since the points of the face that are captured by the
clmtracker make up the coordinates (x,y) that make up the points of the curves. If the user opens the mouth
the clmtracker can capture too big of a differense between two points in the mouth and the code will make the sunglasses 
fall down onto the eyes, and no further. The tracker looks for a whole face and finds the points based on how the 
"standard" face look. The proces of capturing this way is surprisingly fast and easy to understand and apply, but
flawed when you realize that the computer has a hard time taking anomalies, such as my glasses, into account. 
Light, and lack thereof, can also interfere with the process.

Conceptually, the tracker captures a lot of data from the face, but displays a quite anonymous face, though the "liveness"
of the moving face, convinces the user of the capture of their own face. The fact that a simple facial expression such as opening
the mouth can be captured by the computer, is not unnerving, but it makes you think what else can be gathered from how you 
express your face.

Having configured the capture of a simple facial expression helped me understand how more sophisticated 
tracking technologies like facial recognition is possible. It has also made me think about how we as consumers
of digital culture somehow want to, or like to, be captured - to be seen by something or someone. I think it is
closely related to the fact that people like to interact with digital media.
At the same time, learning about the process behind capturing this quite personal data, makes me understand
more clearly how the technology can be exploited. The quite invisible nature of the technology might be the 
reason it is easy to be blind to this darker side.
