
var ctracker;
let move = 1

function preload() {
  // preloads the image
  img = loadImage('Thug-Life-Cool-Glasses-PNG.png');
}

function setup() {

  //web cam capture
  var capture = createCapture();
  capture.size(640,480);
  capture.position(0,0);
  capture.hide();
  var c = createCanvas(640, 480);
  c.position(0,0);

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);
}

function draw() {
  // background(255)
  strokeWeight(4)
  textSize(32);
  text('Open your mouth', 20, 40);

  var positions = ctracker.getCurrentPosition();
  if (positions.length) { //check the availability of web cam tracking
    //makes line between the tracked points of the face:
      //eyebrows, curve
      beginShape();
      curveVertex(positions[19][0],positions[19][1]);
      curveVertex(positions[19][0],positions[19][1]);
      curveVertex(positions[20][0],positions[20][1]);
      curveVertex(positions[21][0],positions[21][1]);
      curveVertex(positions[22][0],positions[22][1]);
      curveVertex(positions[22][0],positions[22][1]);
      endShape();
      beginShape();
      curveVertex(positions[15][0],positions[15][1]);
      curveVertex(positions[15][0],positions[15][1]);
      curveVertex(positions[16][0],positions[15][1]);
      curveVertex(positions[17][0],positions[17][1]);
      curveVertex(positions[18][0],positions[18][1]);
      curveVertex(positions[18][0],positions[18][1]);
      endShape();
      //left eye, curves
        beginShape();
        curveVertex(positions[25][0],positions[25][1]);
        curveVertex(positions[25][0],positions[25][1]);
        curveVertex(positions[64][0],positions[64][1]);
        curveVertex(positions[24][0],positions[24][1]);
        curveVertex(positions[63][0],positions[63][1]);
        curveVertex(positions[23][0],positions[23][1]);
        curveVertex(positions[23][0],positions[23][1]);
        endShape();
        beginShape();
        curveVertex(positions[25][0],positions[25][1]);
        curveVertex(positions[25][0],positions[25][1]);
        curveVertex(positions[65][0],positions[65][1]);
        curveVertex(positions[26][0],positions[26][1]);
        curveVertex(positions[66][0],positions[66][1]);
        curveVertex(positions[23][0],positions[23][1]);
        curveVertex(positions[23][0],positions[23][1]);
        endShape();
        //right eye, curves
          beginShape();
          curveVertex(positions[30][0],positions[30][1]);
          curveVertex(positions[30][0],positions[30][1]);
          curveVertex(positions[69][0],positions[69][1]);
          curveVertex(positions[31][0],positions[31][1]);
          curveVertex(positions[70][0],positions[70][1]);
          curveVertex(positions[28][0],positions[28][1]);
          curveVertex(positions[28][0],positions[28][1]);
          endShape();
          beginShape();
          curveVertex(positions[30][0],positions[30][1]);
          curveVertex(positions[30][0],positions[30][1]);
          curveVertex(positions[68][0],positions[68][1]);
          curveVertex(positions[29][0],positions[29][1]);
          curveVertex(positions[67][0],positions[67][1]);
          curveVertex(positions[28][0],positions[28][1]);
          curveVertex(positions[28][0],positions[28][1]);
          endShape();
          pop();
          //mouth, inner curves (no lips)
          push();
        fill(random(100,255),random(100,255),random(100,255))
        beginShape();
        curveVertex(positions[50][0],positions[50][1]);
        curveVertex(positions[50][0],positions[50][1]);
        curveVertex(positions[58][0],positions[58][1]);
        curveVertex(positions[57][0],positions[57][1]);
        curveVertex(positions[56][0],positions[56][1]);
        curveVertex(positions[44][0],positions[44][1]);
        curveVertex(positions[44][0],positions[44][1]);
        endShape();
        beginShape();
        curveVertex(positions[50][0],positions[50][1]);
        curveVertex(positions[50][0],positions[50][1]);
        curveVertex(positions[59][0],positions[59][1]);
        curveVertex(positions[60][0],positions[60][1]);
        curveVertex(positions[61][0],positions[61][1]);
        curveVertex(positions[44][0],positions[44][1]);
        curveVertex(positions[44][0],positions[44][1]);
        endShape();
        pop();
          //nose, underneath and nose line
          beginShape();
          curveVertex(positions[36][0],positions[36][1]);
          curveVertex(positions[36][0],positions[36][1]);
          curveVertex(positions[42][0],positions[42][1]);
          curveVertex(positions[37][0],positions[37][1]);
          curveVertex(positions[43][0],positions[43][1]);
          curveVertex(positions[38][0],positions[38][1]);
          curveVertex(positions[38][0],positions[38][1]);
          endShape();
          line(positions[33][0],positions[33][1], positions[41][0],positions[41][1]);
          //sunglasses outside of frame
          image(img, positions[27][0]-130, -100+move, 300, 100);
          // if the mouth opens, the sunglasses will move onto the eyes, but not further down
          if (positions[57][1] > positions[60][1]+20 && move < positions[41][1]+20){
            move = move+4
          }

}
}
