/*inspired by:
"If" - Rudyard Kipling and
Love Letters - Christopher Strachey*/


let language;
let life = 50;
let love = 10;
let noLove = 0;

function preload() {
language = loadJSON('words.json');

}

function setup() {
createCanvas(windowWidth,windowHeight);
//frameRate(love);
}

function draw() {
  background(255);
  frameRate(love);
var change_life ='If you can ';
var destroy = ' and not make ';
var disObey = ' your master';
textAlign(CENTER);
translate(0,-height/4);
textFont("Courier New");
textSize(40);
text(change_life + random(language.isCreativity) + destroy + random(language.something) + disObey, width/2,height/2);

// var change_values = 'If you can ';
// var second2 = ' and not make ';
var begin = ' your aim';
text(change_life + random(language.isCreativity) + destroy + random(language.something) + begin, width/2,height/2+life);

var heal = ' with ';
var oh = ' and ';
text(change_life + random(language.isCreativity) + heal + random(language.something) + oh + random(language.something), width/2, height/2+life+life);

var duck = 'And ';
var cherish = ' those two ';
var forever = 's just the same';
text(duck + random(language.creating) + cherish + random(language.something) + forever, width/2,height/2+life+life+life);

push();
var damn = 'You certainly are ';
textSize(60);
text(damn + random(language.isATool), width/2, height/2+life+life+life+life+life);
pop();

}
function mousePressed() {
  noLoop();
}

function mouseReleased() {
  loop();
}
