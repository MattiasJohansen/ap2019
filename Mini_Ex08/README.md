[Link](https://glcdn.githack.com/MattiasJohansen/ap2019/raw/master/Mini_Ex8/empty-example/index.html)

![image](poem.PNG)

I created this program with [Charlotte](https://gitlab.com/charlottehd/ap2019). We wanted to make a generative poem-writer
inspired by the Christopher Strachey's love letter machine and the poem "If" by Rudyard Kipling. This program doesn't just show one poem, but it changes all the time,
so it is difficult to read, unless you hold the mouse down: it will stop looping and you can finally read the poem that you "chose".

In order to make the randomly generated words, we made a JSON file where we made arrays of different types of words (nouns, verbs etc.)
This was one of the times where it was nice to work together, since we could each come up with different words, thereby giving
the poems a more broad "personality". The process was also helped by the fact that we could bounce ideas off of each other and
generate more ideas for the design. It is not always easy to agree on what idea to go with, but I think we handled it well; talking
the different possibilities over and exploring the design by coding together, seeing what each other could come up with.

I interpret our running program as a sort of shell that we hide the code under: the shell *performs* what the machine want to 
express. The aestetic of the program with the *Courier New*-font and the fast writing of words can assosiate it with and old
writing machine in front of a troubled poet that just tries to come up with something but it always ends up with something silly.
At the same time, a similar font is used in coding, giving some reference to what lies beneath. Seeing this shell the user can
interact with the poem and give rest and time to look a the writing to the thundering machine/poet. The user is here performing
in stead of the machine (having to hold the pressure of the mouse click).

Under the shell lies the source code, which we have tried to give some poetic form by changing the names of variables to reflect
the troubles of the source code; it has problems with expressing language, maybe beacuse it is required to be written in a very 
specific form: *"change_life + random(language.isCreativity) + destroy + random(language.something) + begin"* (line 36).
This way the source code becomes animated or "alive" (like the program). The source code becomes a *body* of text in itself that
can be read and interpreted by humans (and no just by the machine). We as programmers cannot detatch ourselves from the source code, 
since we wrote it but, we can more easily put the responsibility of the combination of poems that the computer makes when the 
code is run, on the machine. This creates a sort of limbo-situation since we at the same time gave the machines the options
to make the comibinations.


