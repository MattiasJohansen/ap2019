[Link](https://glcdn.githack.com/MattiasJohansen/ap2019/raw/master/Mini_Ex6/empty-example/index.html)

![image](screenshot.png)

In my game you control the spaceship with the arrow keys. As in space there is no gravity, so the ship will only stop if you provide
force in the opposite direction of the way you are going. This can be done by turning around or pressing the down-arrow key. The 
object of the game is to fly into the bubbles that are flying through space, thereby gaining points. If you fly out of the screen,
you return to the starting point and lose you points. The bubbles will become smaller and move faster as the player gains more
points, making the game progressively more difficult.

The main "object" of the program is the bubbles, whose movement and look are defined as a class. This made it easier to tinker
with the parameters in the funtion setup and draw. The method of making the bubbles continues to comes is through making an *array*
and *push*ing new objects (the bubbles) into it every 45 framecount. The bubble look on the varying sizes of bubbles is achieved
by using the defined this.size and this.pos to make a smaller lighter circle that is relative in size and position to the size
individual bubbles that spawns. 

The object-oriented programming allow you to think of the objects in you game as having different attributes that is specific
of the type of the object. As Daniel Shiffman put it: "What does mean to be a bubble?". Though I did incidentally use bubbles, you can
apply this thinking to all manner of things. So once defined what makes up an object, you can reuse the characteristics and thus
save time. This affordance of re-usability is fundamental to the digital culture, especially that of the open-source community. 
Being able to copy and expand on other peoples code - other people defining and classing of objects, enables creativity and the 
ability to share thoughts and art. But due to the importance of copyright, security and stability, everything cannot be open-source.


