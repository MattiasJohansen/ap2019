[Link to program](https://gl.githack.com/MattiasJohansen/ap2019/raw/master/Mini_Ex2/empty-example/index.html)

![Screenshot](emojis.png)

My program can make the eyebrows of the left emoji jump up, when you click the 
mouse. The right emoji has constantly randomized eyecolor and a curly mouth. At 
first, I wanted to make the eye color change as you clicked the mouse, but I
couldn't figure out how to do that. In stead, I just gave it another frame rate.

In the proces of making the program i have learned more about the different 
functions of the mouseIsPressed, mouseClicked etc. I have also experimented with
geometric shapes: circles and curves. The curves were the most difficult to figure
out (hence the funny shapes of the mouths and eyebrows), but the fact that the
result of my experiments were so unforeseeable, I ended up with some funny shapes.
I also used the mouseX/mouseY to explore what the many diffent values meant. I
found it faster and easier to kind of guess what values i wanted to put in, by 
dragging the mouse around, untill I found a good look. Lastly, I also found out 
about the if/else functions and how to make opacity/alpha (the latter from the 
tutorial video).

I thought the reading was very interesting, since it presented some of the 
"dillemmas" of develeping software to our complex reality. I have learned that 
in the current media and political climate giving people more possibilities (with
emojis as a tool for self-expression can easily be blown up to putting people in
boxes and categoriese. I feel like it is a symptom of the internet and vast 
information stream that people are exposed to with social media. The more people
something is exposed to, chances are that something can offend someone. Maybe 
this ever-including climate will continue to find smaller and smaller minorities
that has to be represented. At the same times this inclusion is critiqued for
being driven by capitalistic purposes and perpetuating stereotypes because an 
emoji can never express the complexety of an individual person.

I have based my emoji designs on the original yellow smileys. I have done so 
because I have grown up with these cartoony representations of emotion and mood.
I also believe that they are most used kind because of their 'neutral' way of
expression. The yellowness could of course be (mis)understood as a racist way of
portaying Asian people, because of old racist cartoons. I beleive the popularity 
around the smileys goes to show that it is a misguided argument. 

The emoji with the "bobbing" eyebrows could be used in a flirty manner. The
pursuit of love/mating has never been a 100 % clean area, so the potential for 
"misuse" is definitely there. The one with the colorful eyes is more weird and
unreal than most emojis, but could be used to express an ambigous feeling or
maybe drunkness (or the influence of other substances). In the end I think how
an emoji is perceived the cultural context will depend on the way people use them
which is something that you as a designer never can control fully. 



