var h =200;//height of the eyebrows
let fr = 10;// framerate


function setup() {
  createCanvas(1200, 700);
  background(200);
  frameRate(fr); //framerate for the random eyecolor
}

function draw() {
  fill(255, 234, 40);
  strokeWeight(10);//thickness of black strokes
  stroke(0);//black stroke
  circle(250, 300, 200);//black circle 1
  circle(250+600, 300, 200);//black circle 2
  stroke(255, 184, 40, 150) //orange with some opacity
  strokeWeight(14);
  circle(250, 300, 195); //the orange circle 1
  circle(250+600, 300, 195); //the orange circle 2
  stroke(0); //black stroke
  strokeWeight(10); //thickness of black strokes
  fill(0,0)// 100 % opacity between curves
  curve(0, 0, 350, 400, 150, 400, 0, 200);// mouth1
  curve(1000, 700, 350+600, 400, 150+600, 400, 500, 200);// mouth2
  curve(-50, 200, 200, h, 150, h, 350, 350);// eyebrow, left
  curve(300, 300, 350, h, 300, h, 350, 350);// eyebrow, right
  fill(255);//eye color
  circle(175, 250, 20) // eye1, left
  circle(325, 250, 20) // eye1, right
  fill(random(255), random(255), random(255)); //eye color 2
  circle(175+600, 250, 20) // eye2, left
  circle(325+600, 250, 20) // eye2, right

  if (mouseIsPressed){
     h = 180; // the height changes when the mouse is pressed
   } else {
     h = 200;// if not, it goes back to the original
   }
 }
