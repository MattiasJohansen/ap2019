
function setup() {
  createCanvas(1200, 700, WEBGL);



}

function draw() {
  background(20,59,100)
  let dirX = (mouseX / width - 0.5) * 2;
  let dirY = (mouseY / height - 0.5) * 2;
  directionalLight(250, 250, 550, -dirX, -dirY, 0.05);
  ambientMaterial(255, 180, 0);
  sphere(mouseX, 50, 50)
  rotateX(frameCount * 0.01);
  rotateZ(frameCount * 0.02);
  ambientMaterial(80, 232, 255);
  box(200);
  ambientLight(150);







}
