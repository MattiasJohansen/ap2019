https://gl.githack.com/MattiasJohansen/ap2019/raw/master/MiniEx1/p5/empty-example/index.html

![Screenshot](MiniEx1.png)



My process was very exploratory, since I just tried some of the examples in the 
reference and applied them to what I had already programmed. I started with box
and got it to rotate, just to try it out.Then I put the directional light on, 
from the mouse. I wanted to make a sphere follow the mouse to be kind of a sun, 
that would shine on the rotating box. My attempt to code this was unsuccessful, 
but by trying, I got some insight and a rather funny result.

I think my coding process was quite different from reading and writing, since I
mostly was copying and modifying code that was already given. I was reading in
the reference what the function of the specific code was, values etc. This was a
kind of reading that is more used when studying a subject, rather than reading
e.g. a novel. The writing is also very diffent from writing "normally", because
the syntax is so different; I am not writing whole sentences as if I was 
communicating with a human. The computer has only a specific way of 
understanding language and there is no "wiggle room" as there is with at human.
A human can understand half fulfilled senteces, body language, context etc. The
fact that the computer has the specific logic of its language, is at the same
time frustrating and exciting, depending on whether or not I succesfully 
communicated what i wanted.

I want to do more coding practice, since I understand that I only learn how it 
works, when I do it. Try, try, try. Of course it has to be combined with some
theoretical knowledge, but if you cant apply the theory it is impossible to 
understand how it really works.

To me code is a "secret" system that can communicate or express something. 
The more you know about the code, the more you can communicate or express. This 
is a kind of abstract impression that I have at the moment, and that might 
change the more i get into coding.
